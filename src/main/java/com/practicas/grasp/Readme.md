# GRASP: General Responsibility Assignment Software Patterns
> Patrones Generales de Software para la Asignación de Responsabilidades

> Patrones de Principios Generales para Asignar Responsabilidades 

> Patrones de Asignación de Responsabilidades 
 
## Introducción 
El diseño de objetos se describe algunas veces como alguna variación de los siguiente: 

> Después de la identificación de los requisitos y de la creación de un modelo del dominio, entonces añada métodos a las clases y defina el paso de mensajes entre los objetos para satisfacer los requisitos. 

Esta descripción no es muy útil !...
 
La decisión de qué método, dónde colocarlos y cómo deberían interactuar los objetos, es muy importante y nada trivial. 

Es una etapa critica, es la parte esencial de lo que entendemos por desarrollar un sistema orientado a objetos.

## Responsabilidades y métodos
Las responsabilidades estan relacionadas con las obligaciones de un objeto en cuanto a su comportamiento. Básicamente, estas responsabilidades son de los siguientes dos tipos:
- Conocer
- Hacer

Entre las responsabilidades de **conocer** de un objeto se encuentran:
- Conocer los datos privados encapsulados.
- Conocer los objetos relacionados.
- Conocer las cosas que puede calcular o derivar.

Entre las responsabilidades de **hacer** de un objeto se encuentran:
- Hacer algo él mismo, como crear un objeto o hacer un calculo.
- Iniciar una acción en otros objetos.
- Controlar y coordinar actividades de otros objetos.

Las responsabilidades se asignan a las clases de los objetos durante el diseño.
Por ejemplo :
> Una **Venta** es la responsable de la creación de una **LineaDeVenta** _(un hacer)_

> Una **Venta** es responsable de saber su Fecha _(un conocer)_

La granularidad de las responsabilidades influye en la conversión de las responsabilidades a clases o métodos. 
Por ejemplo :

> La responsabilidad de proporcionar acceso a las bases de datos relacionales _(granularidad gruesa)_

> La responsabilidad de crear una venta _(granularidad fina)_

Una responsabilidad no es lo mismo que un método, pero los métodos se implementan para llevar a cabo responsabilidades. Las responsabilidades se implementan utilizando métodos que actúan solos o colaboran con otros métodos u objetos. Por ejemplo:

> El calculo del total de una **Venta**

## Patrones GRASP

> **Que son los patrones GRASP ?...**
Describen los principios fundamentales del diseño de objetos y la asignación de responsabilidades, expresados como patrones.

GRASP es un acrónimo, el nombre se eligió para sugerir la importancia de **APRENDER _(grasping en ingles)_** estos principios para diseñar con éxito software orientado a objetos. 

1. Experto en información 
2. Creador
3. Alta cohesión 
4. Bajo acoplamiento
5. Controlador

___
> Comience asignado responsabilidades, estableciendo claramente la responsabilidad.
___

### Experto en información

**Problema**: cual es el principio general de asignación de responsabilidades a los objetos.

**Solución**: asigna una responsabilidad al experto en información, la clase que tiene la información necesaria para realizar la responsabilidad. 

> Si se hace bien los sistemas tienden a ser mas fáciles de entender, mantener y ampliar, y existe mas oportunidades para **reutilizar** componentes en futuras aplicaciones.  

## Creador

**Problema**: quien debería ser el responsable de la creación de una nueva instancia de alguna clase ?

**Solución**: asignar a la clase B la responsabilidad de crear una instancia de la clase A si cumple con uno o mas de los siguientes casos.

1. B agrega objetos de A
2. B contiene objetos de A
3. B registra instancias de objetos de A
4. B tiene datos de incialización que se pasarían a un objeto de A cuando sea creado _(por tanto B es EXPERTO con respecto a la creación de A)_

B es un creador de los objetos A. Si tuviera que elegir dos opciones inclínese por una clase B que agrega o contiene la clase A.

> Si se asigna bien, el diseño puede soportar un bajo acoplamiento, mayor claridad, encapsulación y reutilización. 

## Bajo acoplamiento 
**Problema**: como soportar bajas dependencias, bajo impacto del cambio e incremento de la reutilización ?

**Solución**: asignar una responsabilidad de manera que el acoplamiento permanezca bajo. 

El **acoplamiento** es una medida de la fuerza con que un elemento esta conectado a, tiene conocimiento de, confiá en, otros elementos. 

> Un elemento con bajo o débil acoplamiento no depende de demasiados otros elementos, "demasiados" depende del contexto. 

Una clases con alto acoplamiento, podrían no son deseables, pueden adolecer de los siguientes problemas:

* Los cambios en las clases relacionadas fuerzan cambios locales
* Son difíciles de entender de manera aislada
* Son difíciles de reutilizar puesto que su uso requiere la presencia adicional de las clases de las que depende
 
## Alta cohesión
**Problema**: como mantener la complejidad manejable ?

**Solución**: asignar una responsabilidad de manera que la cohesión permanezca alta.

En cuanto al diseño de objetos, la **cohesión**  _(o de manera mas especifica, la cohesión funcional)_ es una medida de la fuerza con la que se relacionan y del grado de focalización de las responsabilidades de un elemento. 

> Un elemento con responsabilidad altamente relacionada y que no hace una gran cantidad de trabajo, tiene alta cohesión.

Una clase con baja cohesión hace muchas cosas no relacionadas, o hace demasiado trabajo. Tales clases no son convenientes, adolecen de los siguientes problemas:
* Dificiles de entender
* Dificiles de reutilizar 
* Dificiles de mantener 
* Delicadas, constantemente afectadas por los cambios

A menudo, las clases con baja cohesión representan una _granularidad gruesa_ de abstracción, o se les han asignado responsabilidades que beberían haberse delegado en otros objetos. 
___
> En la practica, el nivel de cohesión no se puede considerar de manera aislada a otros principios como los patrones de Experto o Bajo Acoplamiento.
___

## Controlador 

**Problema**: quien debe ser el responsable de gestionar un evento de entrada al sistema ?

**Solución**: asignar la responsabilidad de recibir o manejar un mensaje de evento del sistema a una clase que representa una de las siguientes opciones:
* Representa el sistema global, dispositivo o subsistema _(controlador de fachada)_.
* Representa un escenario de caso de uso en el que tiene lugar el evento del sistema.

Un **Controlador** es un objeto que no pertenece a la interfaz de usuario, responsable de recibir o manejar un evento del sistema. Un Controlador define el método para la operación del sistema.

> Normalmente, un controlador deberia delegar en otros objetos el trabajo que se necesita hacer, coordina o controla las actividades pero no realiza mucho trabajo por si mismo.

